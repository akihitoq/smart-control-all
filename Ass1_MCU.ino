
#include<ESP8266WiFi.h>
#include<PubSubClient.h>
#include <Ticker.h>
#include <EEPROM.h>

Ticker ticker;
#define motor1_ena D0
#define motor1_in1 D1
#define motor1_in2 D2
#define encA D5

#define switchBtn D7

int interruptCounter;
int actual;
int target;

int Kp;
int Ki;
int Kd;

double error = 0.00;
double last_error = 0.00;
double sum_error = 0.00;
double Pwm_output = 0.00;

String dir = "";

//#define resistor D8
int resistorValueSwitch;
int resistorValue;

const char* ssid = "Akihitoq";
const char* password = "Supavit1";

const char* clientSub = "/xeus_01/msg";
const char* clientPubRPS = "/xeus_01/msg_pub_rps";
const char* clientPubSW = "/xeus_01/msg_pub_sw";
const char* clientPubRES = "/xeus_01/msg_pub_res";
const char* clientPubKp = "/xeus_01/msg_pub_kp";
const char* clientPubKi = "/xeus_01/msg_pub_ki";
const char* clientPubKd = "/xeus_01/msg_pub_kd";

#define mqtt_server "mqtt.xeus.dev"
#define mqtt_port 1883

String str;

WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
  Serial.begin(9600);
  EEPROM.begin(512);

  Kp = EEPROM.read(0);
  Ki = EEPROM.read(1);
  Kd = EEPROM.read(2);

  pinMode(switchBtn, INPUT);

  pinMode( motor1_ena , OUTPUT);
  pinMode( motor1_in1 , OUTPUT);
  pinMode( motor1_in2 , OUTPUT);

  pinMode(encA, INPUT_PULLUP);
  attachInterrupt(encA, handleInterrupt, RISING);

  Serial.println();
  Serial.println("Connecting to ");
  Serial.println(ssid);


  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);

  ticker.attach(1.0, timerIsr);
}
String rx2_str = "";
String rx_str = "";
void loop() {
  if (Serial.available() > 0) {
    rx_str = Serial.readString();
    rx2_str = rx_str.substring(0, rx_str.length() - 1);
    Serial.println(rx2_str);
    String kPID = rx2_str.substring(0, 2);
    String value = rx2_str.substring(3);
    int val = value.toInt();

    if (kPID == "Kp") {
      EEPROM.write(0, val);
      EEPROM.commit();
      Kp = EEPROM.read(0);
      //Serial.println(Kp);
    } else if (kPID == "Ki") {
      EEPROM.write(1, val);
      EEPROM.commit();
      Ki = EEPROM.read(1);
      //Serial.println(Ki);
    } else if (kPID == "Kd") {
      EEPROM.write(2, val);
      EEPROM.commit();
      Kd = EEPROM.read(2);
      //Serial.println(Kd);
    }

    rx_str = "";
    rx2_str = "";
    Serial.print("Kp : ");
    Serial.print(EEPROM.read(0));
    Serial.print("  Ki : ");
    Serial.print(EEPROM.read(1));
    Serial.print("  Kd : ");
    Serial.println(EEPROM.read(2));
  }

  if (!client.connected()) {
    if (client.connect("ESP8266Client", "xeuslab", "xeuslab2019")) {
      client.subscribe(clientSub);
    }
  }
  client.loop();
}

void callback(char* topic, byte* payload, unsigned int length) {
  String msg = "";
  int i = 0;
  while (i < length) msg += (char)payload[i++];
  //Serial.println(msg);
  setup_motor(msg);
}

void setup_motor(String str) {
  String m = "";
  if (str.substring(0, 1) == "#") {
    m = str.substring(7, 11);
    dir = str.substring(17);
    if (m.length() > 0) {
      //Serial.println(m);
      target = m.toInt();
      timerIsr();
    }
  } else if (str.substring(0, 2) == "Kp") {
    String value = str.substring(3);
    int val = value.toInt();
    EEPROM.write(0, val);
    EEPROM.commit();
    Kp = EEPROM.read(0);
    
  } else if (str.substring(0, 2) == "Ki") {
    String value = str.substring(3);
    int val = value.toInt();
    EEPROM.write(1, val);
    EEPROM.commit();
    Ki = EEPROM.read(1);

  } else if (str.substring(0, 2) == "Kd") {
    String value = str.substring(3);
    int val = value.toInt();
    EEPROM.write(2, val);
    EEPROM.commit();
    Kd = EEPROM.read(2);

  }

}

void run_motor(String direct) {
  digitalWrite( motor1_ena , HIGH);
  if (direct == "L") {
    analogWrite( motor1_in1, 0);
    analogWrite( motor1_in2, Pwm_output);
  } else {
    analogWrite( motor1_in1, Pwm_output);
    analogWrite( motor1_in2, 0);
  }
}

void handleInterrupt() {
  interruptCounter++;
}

void cal_error() {
  actual = interruptCounter / 2;
  error = target - actual;
  error = error > 50 ? 50 : error;
  error = error < -50 ? -50 : error;
}

void cal_pwm() {
  Pwm_output = abs(error) <= 3 ? Pwm_output : 
  Pwm_output + (error * Kp) + (Ki * sum_error) + (Kd * (error - last_error));
  Pwm_output = target == 0 ? 0 : Pwm_output;
  Pwm_output = Pwm_output > 1023 ? 1023 : Pwm_output;
  Pwm_output = Pwm_output < 0 ? Pwm_output * -1 : Pwm_output;
}

void timerIsr() {
  resistorValueSwitch = digitalRead(switchBtn);
  resistorValue = analogRead(A0);

  cal_error();
  cal_pwm();

  Serial.print("  target : ");
  Serial.print(target);
  Serial.print("  rps : ");
  Serial.print(actual);
  Serial.print("  error : ");
  Serial.print(error);
  Serial.print("  pwm : ");
  Serial.print(Pwm_output);
  Serial.print("[");
  Serial.print((Pwm_output * 100.00) / 1023.00);
  Serial.print("%]");
  Serial.print("  switch : ");
  Serial.print(resistorValueSwitch);
  Serial.print("  resistor : ");
  Serial.println(resistorValue);

  sum_error = last_error + error;
  last_error = error;
  String str_rps = String(actual);
  String str_switch =  String(resistorValueSwitch);
  String str_res = String(resistorValue);

  client.publish(clientPubRPS, str_rps.c_str(), true);
  client.publish(clientPubSW, str_switch.c_str(), true);
  client.publish(clientPubRES, str_res.c_str(), true);

  str = String(Kp);
  client.publish(clientPubKp, str.c_str(), true);
  str = String(Ki);
  client.publish(clientPubKi, str.c_str(), true);
  str = String(Kd);
  client.publish(clientPubKd, str.c_str(), true);

  interruptCounter = 0;
  run_motor(dir);
}
